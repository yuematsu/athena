#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"
#include "../PhotonVertexSelectionAlg.h"
#include "PhotonVertexSelection/DecoratePhotonPointingAlg.h"
#include "PhotonVertexSelection/BuildVertexPointingAlg.h"

DECLARE_COMPONENT( CP::PhotonVertexSelectionTool )
DECLARE_COMPONENT( CP::PhotonPointingTool )
DECLARE_COMPONENT( CP::PhotonVertexSelectionAlg )
DECLARE_COMPONENT( DecoratePhotonPointingAlg )
DECLARE_COMPONENT( BuildVertexPointingAlg )

